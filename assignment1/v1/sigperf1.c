/* 	Author: Benjamin Zaganjori - 212987533
	Assignment: 1
	File: sigperf1.c
	Description: sigperf will create two processes of sigshooter. These two
		processes will send SIGUSR1 signals to each other, N times. Finally,
		all three processes will print their CPU and system times. In this
		version, the parent (sigperf) will create two children processes
		(shooter1 and shooter2. shooter1 will process ID passed as commandline
		argument while shooter2 is unknown (0).
	Date: 12/10/2016 */

#define _GNU_SOURCE
#include<sys/types.h>
#include<sys/wait.h>
#include<stdio.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<stdlib.h>
#include<limits.h>
#include<time.h>

int main(int argc, char *argv[]) {
	struct timespec now, tmstart;
	pid_t shooter1 = 0;
	pid_t shooter2 = 0;
	pid_t wid;
	int value = 100;
	int i;
	char str[10];
	char pid[10];

	clock_gettime(CLOCK_REALTIME, &tmstart);
	double start = (double)clock() /(double) CLOCKS_PER_SEC;

	printf("Starting parent 'sigperf1' process and clock...\n");
	if (argc > 2) {
		fprintf(stderr, "%s: 1 argument maximum, got %d\n", argv[0], argc - 1);
		exit(1);
	}

	if (argc == 2) {
		char* p;
		errno = 0;
		long conv = strtol(argv[1], &p, 10);
		if (errno != 0 || *p != '\0' || conv > INT_MAX || conv < 2) {
			fprintf(stderr, "Error - Invalid input. \n");
			exit(1);
		}
		value = conv;
	}
	sprintf(str, "%d", value);

	printf("Starting first fork...\n");
	shooter1 = fork();
	if (shooter1 < 0) {
		fprintf(stderr, "Error %s - Fork failed. \n", strerror(errno));
		exit(1);
	}
	if (shooter1 == 0) {
		printf("Fork 1 success.\n");
		execlp("./sigshooter1", "1", str, (char *) NULL);
		fprintf(stderr, "%s: execlp failed.\n", strerror(errno));
		exit(1);
	}

	printf("Starting second fork...\n");
	shooter2 = fork();
	if (shooter2 < 0) {
		fprintf(stderr, "Error %s - Fork failed. \n", strerror(errno));
		exit(1);
	}
	if (shooter2 == 0) {
		printf("Fork 2 success.\n");
		sprintf(pid, "%d", shooter1);
		execlp("./sigshooter1", "2", str, pid, (char *) NULL);
		fprintf(stderr, "%s: execlp failed. \n", strerror(errno));
		exit(1);
	}

	// waitpid(shooter1, 0, 0);
	// waitpid(shooter2, 0, 0);

	wid = wait(NULL);
	if (wid == shooter1) printf("Process %d finished\n",wid, argv[0]);
	if (wid == shooter2) printf("Process %d finished\n",wid, argv[0]);
	wid = wait(NULL);
	if (wid == shooter1) printf("Process %d finished\n",wid, argv[0]);
	if (wid == shooter2) printf("Process %d finished\n",wid, argv[0]);

	clock_gettime(CLOCK_REALTIME, &now);
	double end = (double)clock() / (double) CLOCKS_PER_SEC;
	double seconds = (double)((now.tv_sec+now.tv_nsec*1e-9) - (double)(tmstart.tv_sec+tmstart.tv_nsec*1e-9));
	printf("\t\t%d - CPU: %fs \t System: %fs\n", getpid(), end - start, seconds);

	return 0;
}
