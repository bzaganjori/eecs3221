/* 	Author: Benjamin Zaganjori - 212987533
	Assignment: 1
	File: sigshooter1.c
	Description: sigperf will create two processes of sigshooter. These two
		processes will send SIGUSR1 signals to each other, N times. Finally, all
		three processes will print their CPU and system times. In this version,
		the parent (sigperf) will create two children processes (shooter1 and
		shooter2. shooter1 will process ID passed as commandline argument while
		shooter2 is unknown (0).
	Date: 12/10/2016 */

#define _GNU_SOURCE
#include<sys/types.h>
#include<sys/wait.h>
#include<stdio.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<stdlib.h>
#include<limits.h>
#include<time.h>

struct sigaction sa;
struct timespec now, tmstart;
pid_t siblingPID;
pid_t currentPID;
pid_t updatedPID;
sigset_t mask, oldmask;
int sentSignals = 0;
int flag;
int maxSignals;

void sig_handler(int signum, siginfo_t *info, void *ptr) {
	printf("\t%d: Recieved signal %d from %d\n", getpid(), signum, info->si_pid);
	updatedPID = info->si_pid;
	flag = 1;
}

int main(int argc, char *argv[]) {

	clock_gettime(CLOCK_REALTIME, &tmstart);
	double start = (double)clock() /(double) CLOCKS_PER_SEC;

	sa.sa_sigaction = sig_handler;
	sa.sa_flags = SA_SIGINFO;
	sigemptyset(&sa.sa_mask);

	if (argc == 2) {			// This process does not know its sibling initially
		maxSignals = atoi(argv[1]);
		siblingPID = 0;
	} else if (argc == 3) { 	// This process knows its sibling
		maxSignals = atoi(argv[1]);
		siblingPID = atoi(argv[2]);
	} else {
		printf("\tWe got an error lol.\n");
	}

	printf("\tShooter %d || Sibling %d. \n", getpid(), siblingPID);

	if (sigaction(SIGUSR1, &sa, NULL) == -1) {
		fprintf(stderr, "sigaction failed : %s\n.", strerror(errno));
		exit(1);
	}

	if (siblingPID != 0) { 				// THE SECOND PROCESS WILL SHOOT FIRST
		kill(siblingPID, SIGUSR1);
		sentSignals++;
		flag = 0;
		printf("\t%d >> %d.\n", getpid(), siblingPID);
	} else if (siblingPID == 0) {		// ELSE WAIT FOR SIGNAL (process 1)
		sigsuspend(&sa.sa_mask);
		flag = 1;
	} else {
		printf("\tError.\n");
	}

	if (siblingPID == 0) {
		siblingPID = updatedPID;
	}

	while (sentSignals < maxSignals / 2) {
		if (flag == 1) {			// GREEN LIGHT TO SEND
			kill(siblingPID, SIGUSR1);
			printf("\t%d >> %d.\n", getpid(), siblingPID);
			flag = 0;
			sentSignals++;
		} else if (flag == 0) {		// WAIT FOR SIGNAL
			sigprocmask (SIG_BLOCK, &mask, &oldmask);
			sigsuspend(&sa.sa_mask);
			sigprocmask (SIG_UNBLOCK, &mask, NULL);
		} else {
			printf("\tShouldn't be here.\n");
			exit(1);
		}
	}

	if (strcmp(argv[0], "1")) {
		sigprocmask (SIG_BLOCK, &mask, &oldmask);
		sigsuspend(&sa.sa_mask);
		sigprocmask (SIG_UNBLOCK, &mask, NULL);
	}

	clock_gettime(CLOCK_REALTIME, &now);
	double end = (double)clock() / (double) CLOCKS_PER_SEC;
	double seconds = (double)((now.tv_sec+now.tv_nsec*1e-9) - (double)(tmstart.tv_sec+tmstart.tv_nsec*1e-9));
	printf("\t\t%d - CPU: %fs \t System: %fs\n", getpid(), end - start, seconds);

	return 0;
}
