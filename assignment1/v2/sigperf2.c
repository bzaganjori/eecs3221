/* 	Author: Benjamin Zaganjori - 212987533
	Assignment: 1
	File: sigperf2.c
	Description: sigperf will create two processes of sigshooter. These two
		processes will send SIGUSR1 signals to each other, N times. Finally,
		all three processes will print their CPU and system times. In this
		version, the parent (sigperf) will create two children processes
		(shooter1 and shooter2. shooter1 will process ID passed as commandline
		argument while shooter2 is unknown (0).
	Date: 12/10/2016 */

#define _GNU_SOURCE
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<stdio.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<stdlib.h>
#include<limits.h>
#include<time.h>
#include<fcntl.h>

int main(int argc, char *argv[]) {
	struct timespec now, tmstart;
	pid_t shooter1;
	pid_t shooter2;
	pid_t wid;
    int value = 100;
	int i;
	char *piper = "/tmp/piper";
	char maxSignals[10];
	char pid[10];
	char *firstShooter;

    clock_gettime(CLOCK_REALTIME, &tmstart);
    double start = (double)clock() /(double) CLOCKS_PER_SEC;

	/* INPUT VERIFICATION BLOCK ##############################################*/
	if (argc == 2) {
		firstShooter = argv[1];
		if (strcmp(argv[1], "1") != 0 && strcmp(argv[1], "2") != 0) {
			fprintf(stderr, "%s: Input error - Only 1 or 2, got %s. \n", argv[0], argv[1]);
			exit(1);
		}
	} else if (argc == 3) {
		firstShooter = argv[1];
		if (strcmp(argv[1], "1") != 0 && strcmp(argv[1], "2") != 0) {
			fprintf(stderr, "%s: Input error - Only 1 or 2, got %s. \n", argv[0], argv[1]);
			exit(1);
		}
		char* p;
		errno = 0;
		long conv = strtol(argv[2], &p, 10);
		if (errno != 0 || *p != '\0' || conv > INT_MAX || conv < 2) {
			fprintf(stderr, "%s: Input error - Input needs to be an integer > 1. \n", argv[0]);
			exit(1);
		}
		value = conv;
	} else {
		fprintf(stderr, "%s: need 1 or 2 arguments, got %d.\n", argv[0], argc - 1);
		exit(1);
	}
	/*########################################################################*/

	sprintf(maxSignals, "%d", value);

	if (mkfifo(piper, 0666) != 0) {
		fprintf(stderr, "%s: piper creation failed %d.\n", argv[0], errno);
		exit(1);
	}

	printf("Starting first fork...\n");
	shooter1 = fork();
	if (shooter1 < 0) {
		fprintf(stderr, "Error %s - Fork failed. \n", strerror(errno));
		exit(1);
	}
	if (shooter1 == 0) {
		printf("Fork 1 success.\n");
		execlp("./sigshooter2", maxSignals, piper, firstShooter,"1", (char *) NULL);
		fprintf(stderr, "%s: execlp failed.\n", strerror(errno));
		exit(1);
	}

	printf("Starting second fork...\n");
	shooter2 = fork();
	if (shooter2 < 0) {
		fprintf(stderr, "Error %s - Fork failed. \n", strerror(errno));
		exit(1);
	}
	if (shooter2 == 0) {
		printf("Fork 2 success.\n");
		execlp("./sigshooter2", maxSignals, piper, firstShooter, "2", (char *) NULL);
		fprintf(stderr, "%s: execlp failed. \n", strerror(errno));
		exit(1);
	}

	wid = wait(NULL);
	if (wid == shooter1) printf("Process %d (%s) finished\n",wid, argv[1]);
	if (wid == shooter2) printf("Process %d (%s) finished\n",wid, argv[1]);
	wid = wait(NULL);
	if (wid == shooter1) printf("Process %d (%s) finished\n",wid, argv[1]);
	if (wid == shooter2) printf("Process %d (%s) finished\n",wid, argv[1]);

	unlink(piper);

    clock_gettime(CLOCK_REALTIME, &now);
	double end = (double)clock() / (double) CLOCKS_PER_SEC;
	double seconds = (double)((now.tv_sec+now.tv_nsec*1e-9) - (double)(tmstart.tv_sec+tmstart.tv_nsec*1e-9));
    printf("\t\t%d - CPU: %fs \t System: %fs\n", getpid(), end - start, seconds);

	return 0;
}
