/* 	Author: Benjamin Zaganjori - 212987533
	Assignment: 1
	File: sigshooter2.c
	Description: sigperf will create two processes of sigshooter. These two
		processes will send SIGUSR1 signals to each other, N times. Finally, all
		three processes will print their CPU and system times. In this version,
		the parent (sigperf) will create two children processes (shooter1 and
		shooter2. shooter1 will process ID passed as commandline argument while
		shooter2 is unknown (0).
	Date: 12/10/2016 */

#define _GNU_SOURCE
#define MAX_BUF 1024
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<stdio.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<stdlib.h>
#include<limits.h>
#include<time.h>
#include<fcntl.h>

struct sigaction sa;
struct timespec now, tmstart;
sigset_t mask, oldmask;
pid_t siblingPID;
clock_t start;
clock_t end;
int i;
int fd;
int flag = 1;
int maxSignals;
int shootOrder;
int processNumber;
int sentSignals = 0;
char *piper;
char charPID[10];

void sig_handler(int signum, siginfo_t *info, void *ptr) {
	printf("\tSignal %d: from %d\n", getpid(), info->si_pid);
	siblingPID = info->si_pid;
	sprintf(charPID, "%d", siblingPID);
	flag = 1;
}

int main(int argc, char *argv[]) {

	clock_gettime(CLOCK_REALTIME, &tmstart);
	double start = (double)clock() /(double) CLOCKS_PER_SEC;

	sa.sa_sigaction = sig_handler;
	sa.sa_flags = SA_SIGINFO;
	sigemptyset(&sa.sa_mask);

	maxSignals = atoi(argv[0]);
	piper = argv[1];
	shootOrder = atoi(argv[2]);
	processNumber = atoi(argv[3]);

	if (sigaction(SIGUSR1, &sa, NULL) == -1) {
		fprintf(stderr, "sigaction failed : %s\n.", strerror(errno));
		exit(1);
	}

	fd = open(piper, O_RDWR);

	if (processNumber == 1) { 					// THIS IS THE FIRST PROCESS
		if (shootOrder == 1) { 					// THIS PROCESS SHOOTS FIRST
			read(fd, charPID, sizeof(charPID));
			printf("\t%d READ %s\n", getpid(), charPID);
		}
		if (shootOrder == 2) {								// SIBLING SHOOTS FIRST
			sprintf(charPID, "%d", getpid());
			write(fd, charPID, sizeof(charPID));
			printf("\t%d WRITE %s\n", getpid(), charPID);
			flag = 0;
		}
	} else if (processNumber == 2) {			// THIS IS THE SECOND PROCESS
		if (shootOrder == 1) {					// SIBLING SHOOTS FIRST
			sprintf(charPID, "%d", getpid());
			write(fd, charPID, sizeof(charPID));
			printf("\t%d WRITE %s\n", getpid(), charPID);
			flag = 0;
		}
		if (shootOrder == 2) {								// THIS PROCESS SHOOTS FIRST
			read(fd, charPID, sizeof(charPID));
			printf("\t%d: READ %s\n", getpid(), charPID);
			maxSignals++;
		}
	} else {
		printf("\tShould not be here.\n");
		exit(1);
	}

	while (sentSignals < maxSignals / 2) {
		if (flag == 1) {
			kill(atoi(charPID), SIGUSR1);
			printf("\t%d >> %s\n", getpid(), charPID);
			flag = 0;
			sentSignals++;
		} else if (flag == 0) {
			sigprocmask (SIG_BLOCK, &mask, &oldmask);
			sigsuspend(&sa.sa_mask);
			sigprocmask (SIG_UNBLOCK, &mask, NULL);
		} else {
			printf("\tShould not be here.\n");
			exit(1);
		}
	}

	if (processNumber == 1 && shootOrder == 1) {
		sigprocmask (SIG_BLOCK, &mask, &oldmask);
		sigsuspend(&sa.sa_mask);
		sigprocmask (SIG_UNBLOCK, &mask, NULL);	}

	if (processNumber == 2 && shootOrder == 2) {
		sigprocmask (SIG_BLOCK, &mask, &oldmask);
		sigsuspend(&sa.sa_mask);
		sigprocmask (SIG_UNBLOCK, &mask, NULL);
	}

	close(fd);

	clock_gettime(CLOCK_REALTIME, &now);
	double end = (double)clock() / (double) CLOCKS_PER_SEC;
	double seconds = (double)((now.tv_sec+now.tv_nsec*1e-9) - (double)(tmstart.tv_sec+tmstart.tv_nsec*1e-9));
	printf("\t\t%d - CPU: %fs \t System: %fs\n", getpid(), end - start, seconds);

	return 0;
}
