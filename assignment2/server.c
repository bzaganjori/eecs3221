#define _GNU_SOURCE
#include<errno.h>
#include<getopt.h>
#include<limits.h>
#include<pthread.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<time.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

struct Queue {
	int value;
	struct Queue *next;
};

typedef enum {FREE, BUSY} ServerState;

pthread_t *serverThreads;
pthread_t *clientThreads;
pthread_t tickThread;
pthread_mutex_t queueMutex = 		PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex = 			PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t completion_mutex = 	PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t clock_mutex =		PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t completion_cond = 	PTHREAD_COND_INITIALIZER;
pthread_cond_t clock_signal = 		PTHREAD_COND_INITIALIZER;
pthread_cond_t tick = 				PTHREAD_COND_INITIALIZER;
pthread_cond_t finish = 			PTHREAD_COND_INITIALIZER;

struct Queue *head = NULL;
struct Queue *tail = NULL;
int sizeQueue = 	0;
int jobsGenerated = 0;
int completeFlag = 	0;
int clockTick = 	0;
int completion_counter = 0;
int clock_counter =	0;
double lambda = 	0.005;
double mu = 		0.01;
double servers = 	2;
double clients = 	2;

double generateJob();
void *thread_clock(void  *i);
void *thread_server(void *mu);
void *thread_client(void *lambda);
void startup_wait();
void startup_signal();
void completion_wait(int how_many);
void completion_signal();
void lock_mutex();
void wait_for_tick();
void unlock_mutex();
void outputInformation();
void errorCheck(int errNo, char *string);
void enqueue(int value);
void dequeue();

int main(int argc, char **argv) {
	int i;
	int rc;
	int c;

	while (1) {
		static struct option longOpt[] = {
			{"lambda", 	required_argument, 0, 'l'},
			{"mu",		required_argument, 0, 'm'},
			{"servers", required_argument, 0, 's'},
			{"clients", required_argument, 0, 'c'},
			{0,			0,				   0, 0}
		};

		int longIndex = 0;
		c = getopt_long(argc, argv, "l:m:s:c:", longOpt, &longIndex);

		if (c == -1) break;
		switch (c) {
			case 0:
				break;
			case 'l':
				lambda = atof(optarg);
				break;
			case 'm':
				mu = atof(optarg);
				break;
			case 's':
				servers = atof(optarg);
				break;
			case 'c':
				clients = atof(optarg);
				break;
			case '?':
				printf("Invalid input ?\n");
				exit(1);
				break;
			default:
				printf("Invalid input.\n");
				exit(1);
		}

	}

	/* Allocate memory for servers and client threads. */
	clientThreads = malloc(sizeof(pthread_t) * clients);
	serverThreads = malloc(sizeof(pthread_t) * servers);

	/* Populate the client thread array creating the client threads. */
	for (i = 0; i < clients; i++) {
		rc = pthread_create(&clientThreads[i], NULL, thread_client, (void *) &lambda);
		errorCheck(rc, "main - client thread creation");
	}

	/* Populate the server thread array creating the server threads. */
	for (i = 0; i < servers; i++) {
		rc = pthread_create(&serverThreads[i], NULL, thread_server, (void *) &mu);
		errorCheck(rc, "main - server thread creation");
	}

	/* Create thread for clock. */
	rc = pthread_create(&tickThread, NULL, thread_clock, (void *) 0);
	errorCheck(rc, "main - clock thread creation");

	/* Wait for clock thread to complete. */
	pthread_join(tickThread, NULL);

	printf("Total number of jobs generated: %d\n", jobsGenerated);

	return 0;
}

/* Clock tick thread. It will run for 1000 ticks. On each tick, it will
 * signal a condition variable that both the server and client depend on.
 * Once signaled, the server and client will wake up, do their thing
 * and signal back once complete for a new tick. */
void *thread_clock(void *i) {
	int rc;

	while (clockTick < 100) {

		/* Send signal to thread_server and thread_client. */
		printf("CLOCK - SEND SIGNAL\n");
		printf("ITERATION %d\n", clockTick);

		lock_mutex();

		rc = pthread_cond_broadcast(&tick);
		errorCheck(rc, "clock - signal broadcast");

		unlock_mutex();

		completion_wait(servers + clients);

		clockTick++;
	}

	completeFlag = 1;

	pthread_exit(NULL);
}

/** Server thread function. Once the server is created, it will spinlock
 * until it is woken up by the condition variable "tick". Once awake, we
 * check if it is busy or not. If it is busy, continue the spinlock. Else
 * accept an open job and complete it.*/
void *thread_server(void *mu) {
	int rc;
	ServerState state = FREE;

	while(1) {

		startup_signal();

		if (completeFlag == 1) break;

		lock_mutex();
		wait_for_tick();

		if (state == BUSY) {
			if (generateJob() < *(double *) mu) {
				dequeue();
				state = FREE;
			}
		}

		if (state == FREE && sizeQueue != 0) state = BUSY;

		printf("\tSERVER - SEND TO CLOCK.\n");
		completion_signal();

		unlock_mutex();
		completion_signal();
	}

	pthread_exit(NULL);
}

/** Client thread function. */
void *thread_client(void *lambda) {
	int rc;

	while(1) {

		startup_signal();

		if (completeFlag == 1) break;

		lock_mutex();
		wait_for_tick();

		if (generateJob() < *(double *) lambda)
			enqueue(0);

		printf("\tCLIENT - SEND TO CLOCK. \n");
		completion_signal();

		unlock_mutex();
		completion_signal();
	}

	pthread_exit(NULL);
}

/** Wait until all servers and clients are finished so there is no wait hang. */
void startup_wait() {
	pthread_mutex_lock(&clock_mutex);
	if (clock_counter < servers + clients)
		pthread_cond_wait(&clock_signal, &clock_mutex);
	pthread_mutex_unlock(&clock_mutex);
}

/** Once the clock counter has completed, signal for a new tick. */
void startup_signal() {
	pthread_mutex_lock(&clock_mutex);
	clock_counter++;
	if (clock_counter == servers + clients)
		pthread_cond_signal(&clock_signal);
	pthread_mutex_unlock(&clock_mutex);
}

/** */
void completion_wait(int how_many) {
	pthread_mutex_lock(&clock_mutex);
	pthread_cond_wait(&clock_signal, &clock_mutex);
	pthread_mutex_unlock(&clock_mutex);
}

/** Notify the clock that all servers and clients are done. */
void completion_signal() {
	pthread_mutex_lock(&clock_mutex);
	clock_counter--;
	if (!clock_counter)
		pthread_cond_signal(&clock_signal);
	pthread_mutex_unlock(&clock_mutex);
}

/** Abstraction function that locks the main mutex. */
void lock_mutex() {
	int rc;
	rc = pthread_mutex_lock(&mutex);
	errorCheck(rc, "clock - mutex lock");
}

/** Abstraction function that unlocks the main mutex. */
void unlock_mutex() {
	int rc;
	rc = pthread_mutex_unlock(&mutex);
	errorCheck(rc, "server - mutex unlock");
}

/** Abstraction function that waits for clock tick. */
void wait_for_tick() {
	int rc;
	pthread_cond_wait(&tick, &mutex);
	printf("\tSERVER - RECIEVE FROM CLOCK\n");
}

/** Abstraction function that checks for errors. */
void errorCheck(int errNo, char *string) {
	if (errNo) {
		fprintf(stderr, "pthread error %d in function: %s\n", errNo, string);
		exit(1);
	}
}

/** Generate a random real number between 0 and 1. */
double generateJob() {
	jobsGenerated++;
	return ((double) rand() / (double) (RAND_MAX));
}

/** The following are two simple queue operations - enqueue and dequeue.
 * Since we have multiple servers running, we must not allow two servers
 * to accept the same job or mess up the enqueue and dequeue to the same
 * memory address. We introduce a mutex to the dequeue operation. Lock
 * before critical section and then unlock mutex.*/
void enqueue(int value) {
	int rc;
	struct Queue *temp = (struct Queue*) malloc(sizeof(struct Queue));
	temp->value = value;
	temp->next = NULL;

	/* Lock mutex and add new job to queue. */
	rc = pthread_mutex_lock(&queueMutex);
	errorCheck(rc, "enqueue - mutex lock");

	if (head == NULL) {
		head = tail = temp;
		return;
	} else {
		tail->next = temp;
		tail = temp;
		sizeQueue++;
	}

	/* Unlock mutex. */
	rc = pthread_mutex_unlock(&queueMutex);
	errorCheck(rc, "enqueue - mutex unlock");
}

void dequeue() {
	int rc;

	/* Lock mutex and remove completed job */
	rc = pthread_mutex_lock(&queueMutex);
	errorCheck(rc, "dequeue - mutex lock");

	if (head == NULL) {
		return;
	} else {
		head = head->next;
		sizeQueue--;
	}

	/* Unlock mutex. */
	rc = pthread_mutex_unlock(&queueMutex);
	errorCheck(rc, "dequeue - mutex unlock");
}
