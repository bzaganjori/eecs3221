#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <errno.h>
#include "mapping.h"

int my_cnt(char *dir) {
	int fd, i, status, num = 0;
	struct stat s;
	char *data, c;
	size_t size;

	/* Open specified file. */
	if ((fd = open(dir, O_RDONLY)) == -1) {
		perror("open");
		return 1;
	}

	/* Get status of file. */
	if ((status = fstat(fd, &s)) < 0) {
		perror("fstat");
		return 1;
	}

	/* Get file size and map to memory. */
	size = s.st_size;
	data = (char *) mmap(0, size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (data == (caddr_t)-1) {
		perror("mmap");
		return 1;
	}

	/* Saerch for new line characters. */
	for (i = 0; i < size; i++) {
		c = data[i];
		if (c == '\n') num++;
	}

	close(fd);
	printf("There are %d new lines from MAPPING. \n", num);
	return num;
}
