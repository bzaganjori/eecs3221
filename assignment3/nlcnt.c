#define _GNU_SOURCE
#include<errno.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<dlfcn.h>
#include<sys/mman.h>
#include<fcntl.h>

typedef int (*func)(char* dir);

int main(int argc, char *argv[]) {
	void *module;

	if (argc != 3) {
		fprintf(stderr, "%s: Expecting two arguments.\n", argv[0]);
		printf("./nlcnt [reading/mapping] [file directory]\n");
		exit(1);
	}

	if (strcmp("reading", argv[1]) == 0) {
		module = dlopen("reading.so", RTLD_LAZY);
	} else if (strcmp("mapping", argv[1]) == 0) {
		module = dlopen("mapping.so", RTLD_LAZY);
	} else {
		fprintf(stderr, "%s: Invalid input - mapping or reading.\n", argv[0]);
		exit(1);
	}

	if (!module) {
		fprintf(stderr, "Couldn't find shared library: %s\n", dlerror());
		exit(1);
	}

	int (*func)(char *x) = dlsym(module, "my_cnt");
	(*func)(argv[2]);

	dlclose(module);
	return 0;
}
