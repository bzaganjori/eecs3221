#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <errno.h>
#include "reading.h"

int my_cnt(char *dir) {
	int fd, i, num = 0, pagesize;
	char *data, c;

	/* Open specified file. */
	if ((fd = open(dir, O_RDWR)) < 0) {
		perror("open");
		exit(1);
	}

	pagesize = getpagesize();

	/* Map to file in memory. */
	data = mmap((caddr_t)0, pagesize, PROT_READ, MAP_PRIVATE, fd, 0);
	if (data == (caddr_t)(-1)) {
		perror("mmap");
		exit(1);
	}

	/* Search for new line characters. */
	for (i = 0; i < pagesize; i++) {
		c = data[i];
		if (c == '\n') num++;
	}

	close(fd);
	printf("There are %d new lines from READING.\n", num);
	return num;
}
